import React from "react";
import { Button } from "../lib/fishpond-button";

export default { title: "Button" };

export const primary = () => <Button>Hello Button</Button>;
